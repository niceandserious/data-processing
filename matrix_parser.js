// allows us to run es6 scripts via node
require("babel-register")({
  presets: ["env"]
});

var args = process.argv.slice(2);
// path to the input xlsx file
var path = args[0];
if (!path) {
  console.error("please provide the path to the xlsx file for this import");
  return;
}

// the sector this import represents
var sector = args[1];
if (!sector) {
  console.error("please provide the sector for this import e.g. agriculture");
  return;
}
if (["agriculture", "buildings", "industry", "power", "transport"].indexOf(sector) < 0) {
  console.error("valid sector options are agriculture, buildings, industry and power");
  return;
}

// the technology this import represents
var tech = args[2];
if (!tech) {
  console.error("please provide the technology for this import e.g. tillage");
  return;
}
tech = tech.toLowerCase();

// the data tab the import represents
var tab = args[3];
if (!tab) {
  console.error(
    "please provide the data tab for this import e.g. combined-scenario"
  );
  return;
}

// whether to fully replce existing file on s3
var force_replace = args[4];

if (
  [
    "combined-scenario",
    "uncertainty-historical",
    "uncertainty-expert",
    "combined-uncertainty"
  ].indexOf(tab) < 0
) {
  console.error(
    "valid sector options are combined-scenario, uncertainty-historical, uncertainty-expert and combined-uncertainty"
  );
  return;
}

var parser;
if (tab == "combined-scenario") {
  parser = require("./src/matrix_combined_scenario_parser.js").default;
} else {
  // we're dealing with uncertainty data, handled differently TBC
  parser = require("./src/matrix_uncertainty_parser.js").default;
}

var s3_upload = require("./src/s3_upload.js").default;
var filename = "data-" + sector + ".json";
// grab existing json from S3 if possible
s3_upload.download(filename).then(function(existing) {
  var output = {};
  if (!force_replace) {
    output = existing;
  }

  // parse the input file
  output[tech] = parser.processFile(path, tab, output[tech] || {});
  // upload file to S3
  s3_upload.upload(filename, JSON.stringify(output));
});
