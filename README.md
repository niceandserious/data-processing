# INNOPATHS xlsx import scripts

## requirements

```
node >= 9.10.1
```

## installation

run the following from the root of the project

```
npm install
```

## configure

Make a copy of `config.json.example` to `config.json` with your AWS access key and secret key. For testing purposes be sure to change the s3_bucket path from "innopaths-data" to "innopaths-test-data"

```
{
	"aws_access_key_id": "your access key",
	"aws_secret_access_key": "your secret key",
	"s3_bucket": "innopaths-test-data"
}
```

## TECHNOLOGY MATRIX use

To parse a file for Technology Matrix run the matrix_parser.js script with a path to the xlsx file, sector, technology and data tab as arguments:

```
node matrix_parser.js input/solar.xlsx agriculture tillage combined-scenario
```

```
node matrix_parser.js input/uncertainty.xlsx agriculture tillage uncertainty-historical
```

The technology name should be written in kebab case if longer than a single word e.g. biomass-ccs-final.

To completely overwrite a sector json file (e.g. data-agriculture.json) instead of intelligently merging data, provide a true parameter at the end of the command

```
node matrix_parser.js input/solar.xlsx agriculture tillage combined-scenario true
```

## LCPP use

To parse a file for LCPP run

```
node lcpp_parser.js input/lcpp/lcpp-data.xlsx input/lcpp/lcpp-metadata.xlsx
```

## Data Format for LCPP

- CSV's should be converted to XLSX format
- Currently, the 'Variables' column needs to be put into the first position
- It seems to work when cells are empty, but if there are problems, empty cells might need to be converted to `0`.

## File Formats

- XLSX file format supported
- Files are automatically uploaded to S3
