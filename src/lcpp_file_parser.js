import xlsx from 'node-xlsx'

// key value pairs of output fields mapped to their input idx
const t = {
  variable: 0,
  model: 1,
  scenario: 2,
  region: 3,
  unit: 4,
  data: 5 //onwards
}

const m = {
  variable: 0,
  preferred_unit: 1,
  type: 2,
  parent: 3,
  display: 4,
  description: 5,
  color: 6,
  etm_ucl: 7,
  primes: 8,
  remind_2p0: 9,
}


// use this variable to give each variable a unique id key
let varId = 0

export default {
  output: {},
  heirachy: [],
  timestamps: [],
  processFile (path, meta_path) {
    // parses the file at provided path, normalizes the data and pushes to s3
    let meta_sheets = this.parseFile(meta_path)
    let meta_data = this.processMeta(meta_sheets[0])
    let work_sheets = this.parseFile(path)
    work_sheets.forEach( (work_sheet) => {
      this.processSheet(work_sheet, meta_data)
    })
    this.heirachy.forEach((branch) => {
      this.pruneChildren(branch)
    })
    let variables = this.processVariables(this.output)
    return {
      data: this.output,
      heirachy: this.heirachy,
      variables: variables
    }
  },
  parseFile (path) {
    // parses an excel file at provided path into an array of worksheets
    let work_sheets = xlsx.parse(path)
    return work_sheets
  },
  processMeta (sheet) {
    let meta_data = {}
    sheet.data.forEach( (row, idx) => {
      if (idx == 0) return
      // only include rows with display set to true
      if (!row[m.display]) return
      meta_data[row[m.variable]] = {
        preferredUnit: row[m.preferred_unit],
        description: row[m.description],
        type: row[m.type],
        color: row[m.color],
        models: []
      }
      if (row[m.etm_ucl]) {
        meta_data[row[m.variable]].models.push('ETM-UCL');
      }
      if (row[m.primes]) {
        meta_data[row[m.variable]].models.push('PRIMES');
      }
      if (row[m.remind_2p0]) {
        meta_data[row[m.variable]].models.push('REMIND 2.0');
      }
    })
    return meta_data
  },
  processSheet (sheet, meta_data) {
    sheet.data.forEach( (row, idx) => {
      if (idx == 0)
        return this.processHeader(row)
      this.processRow(row, meta_data)
      this.processHeirachy(row, meta_data)
    })
  },
  processHeader (row) {
    // loop over the date portion of the header and create and
    // object mapping column indexes to timestamps
    row.slice(t.data).forEach( (cell) => {
      this.timestamps.push((new Date(`${cell}-01-01`)).getTime())
    })
  },
  processRow (row, meta_data) {
    // loop over a row and process it using the template

    // get or set the top level variable object
    let variable = row[t.variable]
    if (!variable || !meta_data[variable])
      return
    // variable = variable.toLowerCase()

    let var_arr = this.output[variable] = this.output[variable] || []

    // TW test code
    const rowMeta = meta_data[variable]

    var_arr.push(this.processRowData(row, variable, rowMeta))
  },
  processHeirachy (row, meta_data) {
    //console.log(this.heirachy)
    let variable = row[t.variable]
    if (!variable || !meta_data[variable])
      return
    // variable = variable.toLowerCase()
    let tree = variable.split('|')
    let branch = this.heirachy
    tree.forEach( (node, idx) => {
      if (typeof(branch) == 'undefined')
        return
      let match = branch.find( (b) => {
        return b.name == node
      })
      if (!match) {
        match = {
          id: varId,
          name: node,
          children: []
        }
        varId++
        if (idx == tree.length-1) {
          match.path = variable
          // merge the object with the relevant metadata
          Object.assign(match, meta_data[variable])
        }
        branch.push(match)
      }
      branch = match.children
    })
  },
  pruneChildren (obj) {
    if (typeof(obj.children) == 'undefined')
      return
    if (!obj.children.length) {
      delete(obj.children)
    } else {
      obj.children.forEach((child) => {
        this.pruneChildren(child)
      })
    }
  },
  processRowData (row, variable, rowMeta) {
    // given a row, creates pairs of timestamps and data points
    let data = {
      MODEL: row[t.model],
      SCENARIO: row[t.scenario],
      REGION: row[t.region],
      UNIT: row[t.unit],
      // TW test code. How do we name this to differentiate when it comes to comparing data series in Highcharts?
      // TYPE: `${rowMeta.type}`
      // TYPE: `${row[t.model]} - ${rowMeta.type}`
    }
    let temporal_data = row.slice(t.data).map( (cell, idx) => {
      return this.processCellData(cell, idx)
    })
    temporal_data.forEach((t_data)=>{
      if (t_data && t_data.length && t_data[0]) {
        data[t_data[0]] = t_data[1]
      }
    })
    return data
  },
  processCellData (cell, idx) {
    return [this.timestamps[idx], cell]
  },
  processVariables (output) {
    let variables = {
      models: [],
      scenarios: [],
      regions: []
    }
    // loop through the output and generate unique model, scenario and region variables
    for (let variable in output) {
      output[variable].forEach((data) => {
        if (variables.models.indexOf(data.MODEL) == -1)
          variables.models.push(data.MODEL)
        if (variables.scenarios.indexOf(data.SCENARIO) == -1)
          variables.scenarios.push(data.SCENARIO)
        if (variables.regions.indexOf(data.REGION) == -1)
          variables.regions.push(data.REGION)
      })
    }
    return variables
  }
}
