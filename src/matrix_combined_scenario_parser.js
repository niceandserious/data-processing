import xlsx from "node-xlsx";

// key value pairs of output fields mapped to their input idx
const t = {
  model: 0,
  scenario: 2,
  region: 3,
  variable: 4,
  unit: 5,
  data: 6 //onwards
};

export default {
  output: {},
  timestamps: [],
  tab: null,
  seen_variables: [],
  convertToKebabCase(str) {
    let result = str;

    // Convert camelCase capitals to kebab-case.
    result = result.replace(/([a-z][A-Z])/g, function(match) {
      return match.substr(0, 1) + "-" + match.substr(1, 1).toLowerCase();
    });

    // Convert non-camelCase capitals to lowercase.
    result = result.toLowerCase();

    // Convert non-alphanumeric characters to hyphens
    result = result.replace(/[^-a-z0-9]+/g, "-");

    // Remove hyphens from both ends
    result = result.replace(/^-+/, "").replace(/-$/, "");

    return result;
  },
  processFile(path, tab, data) {
    // parses the file at provided path, normalizes the data and pushes to s3
    this.tab = tab;
    this.output = data;

    // clear out existing data for this tab
    for (var key in this.output) {
      if (this.output.hasOwnProperty(key) && this.output[key][tab]) {
        delete this.output[key][tab];
      }
      // if variable is now empty, delete entirely
      var tab_found = false;
      for (var prop in this.output[key]) {
        if (this.output[key].hasOwnProperty(prop) && prop != "unit") {
          tab_found = true;
        }
      }
      if (!tab_found) {
        delete this.output[key];
      }
    }

    let work_sheets = this.parseFile(path);
    work_sheets.forEach(work_sheet => {
      this.processSheet(work_sheet);
    });
    return this.output;
  },
  parseFile(path) {
    // parses an excel file at provided path into an array of worksheets
    let work_sheets = xlsx.parse(path);
    return work_sheets;
  },
  processSheet(sheet) {
    sheet.data.forEach((row, idx) => {
      if (idx == 0) return this.processHeader(row);
      this.processRow(row);
    });
  },
  processHeader(row) {
    // loop over the date portion of the header and create and
    // object mapping column indexes to timestamps
    row.slice(t.data).forEach(cell => {
      this.timestamps.push(new Date(`${cell}-01-01`).getTime());
    });
  },
  processRow(row) {
    // loop over a row and process it using the template

    // get or set the top level variable object
    let variable = row[t.variable];
    if (!variable) return;
    variable = this.convertToKebabCase(variable);
    this.cleanExisting(variable);

    let unit = row[t.unit];
    let var_obj = (this.output[variable] = this.output[variable] || {});
    let tab_obj = (var_obj[this.tab] = var_obj[this.tab] || {
      models: [],
      unit: unit
    });

    // get or set the data for this tab
    let model = this.getModel(row, tab_obj);
    let data = this.processRowData(row);

    // add the scenario for this row to the model
    model.scenarios.push({
      scenario: row[t.scenario],
      data: data
    });
  },
  cleanExisting(variable) {
    // remove any entries for this tab and variable currently in
    // the output on first run through (this is so we replace the contents
    // of existing s3 file correctly without destroying untouched data)
    if (this.seen_variables.indexOf(variable) == -1) {
      this.seen_variables.push(variable);
      if (this.output[variable] && this.output[variable][this.tab]) {
        delete this.output[variable][this.tab];
      }
    }
  },
  getModel(row, tab_obj) {
    // check if there is an existing model object
    let model = tab_obj.models.find(model => {
      return model.name == row[t.model];
    });
    if (!model) {
      // no matching model was found, create model object
      // and add it to the models list for this tab
      model = {
        name: row[t.model],
        source: "",
        scenarios: []
      };
      tab_obj.models.push(model);
    }
    return model;
  },
  processRowData(row) {
    // given a row, creates pairs of timestamps and data points
    let data = row.slice(t.data).map((cell, idx) => {
      return this.processCellData(cell, idx);
    });
    // filter out null rows
    return data.filter(data => {
      return data;
    });
  },
  processCellData(cell, idx) {
    return [this.timestamps[idx], cell];
  }
};
