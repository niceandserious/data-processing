import AWS from 'aws-sdk'
import fs from 'fs'
let config = JSON.parse(fs.readFileSync("config.json"));
let bucket = config.s3_bucket

AWS.config.update(
  {
    accessKeyId: config.aws_access_key_id,
    secretAccessKey: config.aws_secret_access_key
  }
)

export default {
  download (filename) {
    // download json file
    console.log(`Downloading ${filename} from ${bucket}`)
    let s3 = new AWS.S3()
    let options = {
        Bucket: bucket,
        Key: filename,
    }
    return new Promise(function (resolve, reject) {
      s3.getObject(options, (err, data) => {
        if (err) {
          console.log(`no existing file for ${filename} found in ${bucket}`)
          resolve({})
        }
        if (!data)
          return resolve({})  
        console.log(`Successfully downloaded ${filename} from ${bucket}`)
        resolve(JSON.parse(data.Body.toString()))
      })
    })
  },
  upload (filename, json) {
    // upload json string
    console.log(`Uploading ${filename} to ${bucket}`)
    let s3 = new AWS.S3()
    let params = {
      Bucket: bucket,
      Key: filename,
      Body: json
    }
    s3.putObject(params, (err, data) => {
      if (err) {
        return console.log(err)
      }
      console.log(`Successfully uploaded ${filename} to ${bucket}`)
    })
  }
}