import xlsx from "node-xlsx";

// key value pairs of output fields mapped to their input idx
const t = {
  aggregator: 0,
  function: 1,
  data: 2 //onwards
};

export default {
  output: {},
  timestamps: [],
  tab: null,
  variable: null,
  source: null,
  unit: null,
  attributes_idx: 0,
  processFile(path, tab, data) {
    // parses the file at provided path, normalizes the data and pushes to s3
    this.tab = tab;
    this.output = data;

    // clear out existing data for this tab
    for (var param in this.output) {
      if (this.output.hasOwnProperty(param) && this.output[param][tab]) {
        delete this.output[param][tab];
      }
      // if variable is now empty, delete entirely
      var tab_found = false;
      for (var prop in this.output[param]) {
        if (this.output[param].hasOwnProperty(prop) && prop != "unit") {
          tab_found = true;
        }
      }

      if (!tab_found) {
        delete this.output[param];
      }
    }

    let work_sheets = this.parseFile(path);
    work_sheets.forEach(work_sheet => {
      this.processSheet(work_sheet);
    });
    return this.output;
  },

  parseFile(path) {
    // parses an excel file at provided path into an array of worksheets
    let work_sheets = xlsx.parse(path);
    return work_sheets;
  },

  processSheet(sheet) {
    sheet.data.forEach((row, idx) => {
      if (this.processAttributes(row) && !this.attributes_idx) {
        this.attributes_idx = idx;
      }
    });
    this.cleanExisting();
    sheet.data.forEach((row, idx) => {
      if (idx == 0) return this.processHeader(row);
      if (idx >= this.attributes_idx) return;
      this.processRow(row);
    });
  },

  processAttributes(row) {
    // the unit, source and variable for the whole import are single
    // fields in the input file
    if (!row[0]) return;
    let row_0 = row[0].trim().toLowerCase();
    if (row_0 == "unit") return (this.unit = row[1]);
    if (row_0 == "source") return (this.source = row[1]);
    if (row_0 == "variable") return (this.variable = row[1].toLowerCase());
  },

  processHeader(row) {
    // loop over the date portion of the header and create and
    // object mapping column indexes to timestamps
    row.slice(t.data).forEach(cell => {
      this.timestamps.push(new Date(`${cell}-01-01`).getTime());
    });
  },

  processRow(row) {
    // loop over a row and process it using the template

    // get or set the top level variable object
    let var_obj = (this.output[this.variable] =
      this.output[this.variable] || {});
    let tab_obj = (var_obj[this.tab] = var_obj[this.tab] || {
      models: [],
      unit: this.unit,
      source: this.source,
    });

    // get or set the data for this tab
    let model = this.getModel(row, tab_obj);
    let data = this.processRowData(row);

    // add the scenario for this row to the model
    model.scenarios.push({
      scenario: row[t.aggregator],
      data: data
    });
  },

  cleanExisting() {
    // remove any entries for this tab and variable currently in
    // the output on first run through (this is so we replace the contents
    // of existing s3 file correctly without destroying untouched data)
    if (this.output[this.variable] && this.output[this.variable][this.tab]) {
      delete this.output[this.variable][this.tab];
    }
  },

  getModel(row, tab_obj) {
    // check if there is an existing model object
    let model = tab_obj.models.find(model => {
      return model.name == row[t.function];
    });
    if (!model) {
      // no matching model was found, create model object
      // and add it to the models list for this tab
      model = {
        name: row[t.function],
        scenarios: []
      };
      tab_obj.models.push(model);
    }
    return model;
  },

  processRowData(row) {
    // given a row, creates pairs of timestamps and data points
    let data = row.slice(t.data).map((cell, idx) => {
      return this.processCellData(cell, idx);
    });
    // filter out null rows
    return data.filter(data => {
      return data;
    });
  },

  processCellData(cell, idx) {
    return [this.timestamps[idx], cell];
  }
};
