// allows us to run es6 scripts via node
require('babel-register')({
    presets: [ 'env' ]
});

var args = process.argv.slice(2);
// path to the input xlsx file
var path = args[0];
if (!path) {
  console.error('please provide the path to the xlsx file for this import');
  return;
}

var meta_path = args[1];
if (!meta_path) {
  console.error('please provide the path to the metadata xlsx file for this import');
  return;
}

// parse the input file
var parser = require('./src/lcpp_file_parser.js').default;
var output = parser.processFile(path, meta_path);

// upload file to S3
var s3_upload = require('./src/s3_upload.js').default;

// split data into multiple files
// S3 has filename restrictions, replace '|' and ' ' with '_' and remove '/' characters
for (let variable in output.data) {
  var name = variable.replace(/\|/g, '_').replace(/ /g, '_').replace(/\//g, '');
  var filename = `lcpp-${name}.json`.toLowerCase();
  s3_upload.upload(filename, JSON.stringify(output.data[variable]));
}

// upload heirachy
var filename = 'lcpp-tree.json';
s3_upload.upload(filename, JSON.stringify(output.heirachy));

// upload variables
var filename = 'lcpp-variables.json';
s3_upload.upload(filename, JSON.stringify(output.variables));

